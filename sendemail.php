
<?php

#####################################################

$postData = $uploadedFile = $statusMsg = '';
$msgClass = 'errordiv';
if(isset($_POST['submitcv'])){
	// Get the submitted form data
	$postData = $_POST;
	$email = $_POST['email'];
	$name = $_POST['name'];
	$subject = $_POST['subject'];
	$message = $_POST['message'];
	$mobile=$_POST['mobile'];

	// Check whether submitted data is not empty
	if(!empty($email) && !empty($name) && !empty($mobile)){

		// Validate email
		if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
			$statusMsg = 'Please enter your valid email.';
		}else{
			$uploadStatus = 1;

			// Upload attachment file
			if(!empty($_FILES["attachment"]["name"])){

				// File path config
				$targetDir = "uploads/";
				$fileName = basename($_FILES["attachment"]["name"]);
				$targetFilePath = $targetDir . $fileName;
				$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

				// Allow certain file formats
				$allowTypes = array('pdf', 'doc', 'docx', 'jpg', 'png', 'jpeg');
				if(in_array($fileType, $allowTypes)){
					// Upload file to the server
					if(move_uploaded_file($_FILES["attachment"]["tmp_name"], $targetFilePath)){
						$uploadedFile = $targetFilePath;

					}else{
						$uploadStatus = 0;
						$statusMsg = "Sorry, there was an error uploading your file.";
					}
				}else{
					$uploadStatus = 0;
					$statusMsg = 'Sorry, only PDF, DOC, JPG, JPEG, & PNG files are allowed to upload.';
				}
			}

			if($uploadStatus == 1){

				// Recipient
				$toEmail = 'admin@olineit.com';

				// Sender
				$from = 'noreply@olineit.com';
				$fromName = 'Oline IT';

				// Subject
				$emailSubject = 'New Application Submitted by '.$name;

				// Message
				$htmlContent = '
                    <p><b>Name:</b> '.$name.'</p>
                    <p><b>Email:</b> '.$email.'</p>
                    <p><b>Mobile:</b> '.$mobile.'</p>';

				// Header for sender info
				$headers = "From: $fromName"." <".$from.">";

				if(!empty($uploadedFile) && file_exists($uploadedFile)){

					// Boundary
					$semi_rand = md5(time());
					$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

					// Headers for attachment
					$headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";

					// Multipart boundary
					$message = "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"UTF-8\"\n" .
						"Content-Transfer-Encoding: 7bit\n\n" . $htmlContent . "\n\n";

					// Preparing attachment
					if(is_file($uploadedFile)){
						$message .= "--{$mime_boundary}\n";
						$fp =    @fopen($uploadedFile,"rb");
						$data =  @fread($fp,filesize($uploadedFile));
						@fclose($fp);
						$data = chunk_split(base64_encode($data));
						$message .= "Content-Type: application/octet-stream; name=\"".basename($uploadedFile)."\"\n" .
							"Content-Description: ".basename($uploadedFile)."\n" .
							"Content-Disposition: attachment;\n" . " filename=\"".basename($uploadedFile)."\"; size=".filesize($uploadedFile).";\n" .
							"Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
					}

					$message .= "--{$mime_boundary}--";
					$returnpath = "-f" . $email;


					##################################################################

					$emailcontent=<<<EMAIL
    <!DOCTYPE html>
    <html>
    <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Email Confirmation:</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
  /**
   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.
   */
  @media screen {
    @font-face {
      font-family: 'Source Sans Pro';
      font-style: normal;
      font-weight: 400;
      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');
    }
    @font-face {
      font-family: 'Source Sans Pro';
      font-style: normal;
      font-weight: 700;
      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');
    }
  }
  /**
   * Avoid browser level font resizing.
   * 1. Windows Mobile
   * 2. iOS / OSX
   */
  body,
  table,
  td,
  a {
    -ms-text-size-adjust: 100%; /* 1 */
    -webkit-text-size-adjust: 100%; /* 2 */
  }
  /**
   * Remove extra space added to tables and cells in Outlook.
   */
  table,
  td {
    mso-table-rspace: 0pt;
    mso-table-lspace: 0pt;
  }
  /**
   * Better fluid images in Internet Explorer.
   */
  img {
    -ms-interpolation-mode: bicubic;
  }
  /**
   * Remove blue links for iOS devices.
   */
  a[x-apple-data-detectors] {
    font-family: inherit !important;
    font-size: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
    color: inherit !important;
    text-decoration: none !important;
  }
  /**
   * Fix centering issues in Android 4.4.
   */
  div[style*="margin: 16px 0;"] {
    margin: 0 !important;
  }
  body {
    width: 100% !important;
    height: 100% !important;
    padding: 0 !important;
    margin: 0 !important;
  }
  /**
   * Collapse table borders to avoid space between cells.
   */
  table {
    border-collapse: collapse !important;
  }
  a {
    color: #1a82e2;
  }
  img {
    height: auto;
    line-height: 100%;
    text-decoration: none;
    border: 0;
    outline: none;
  }
  </style>

</head>
<body style="background-color: #e9ecef;">

  <!-- start preheader -->
  <div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;">
   Dear $full_name, Your registration request has been received.
  </div>
  <!-- end preheader -->

  <!-- start body -->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">

    <!-- start logo -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
          <tr>
            <td align="center" valign="top" style="padding: 36px 24px;">
              <a href="http://bbts.net" target="_blank" style="display: inline-block;">
                <img src="img/logo/logo-new.png" alt="Logo" border="0" width="" >
              </a>
            </td>
          </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end logo -->

    <!-- start hero -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;">
              <h1 style="margin: 0; font-size: 32px; font-weight: 600; letter-spacing: -1px; line-height: 48px;">New Applcation Submitted</h1>
            </td>
          </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end hero -->

    <!-- start copy block -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">
              <p style="margin: 0;">

              New Application has been received with attachment and following information:</p>
             $htmlContent

            </td>
          </tr>
          <!-- end copy -->

          <!-- start button -->
          <tr>
            <td align="left" bgcolor="#ffffff">
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                  <td align="center" bgcolor="#ffffff" style="padding: 12px;">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td align="center" bgcolor="#1a82e2" style="border-radius: 6px;">

                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <!-- end button -->

          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">
              <p> </p>
              <p style="margin: 0;"> </p>

            </td>
          </tr>
          <!-- end copy -->
          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf">
            <br>
<pre>
Ispahani Building (5th Floor)
Agrabad C/A, Chittagong-4100.
Fax: +880 31 710434
Phone: +88031714126, 2520550, 2520556.
info@bbts.net
<a style="text-decoration: none; color: rgb(0, 0, 0);" target="_blank" href="http://bbts.net/">www.bbts.net</a><br>
</pre>
               </span><br>
            </td>
          </tr>
          <!-- end copy -->

        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end copy block -->

    <!-- start footer -->
    <tr>
      <td align="center" bgcolor="#e9ecef" style="padding: 24px;">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

          <!-- start permission -->
          <tr>
            <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;">

            </td>
          </tr>
          <!-- end permission -->

          <!-- start unsubscribe -->
          <tr>
            <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;">

            </td>
          </tr>
          <!-- end unsubscribe -->

        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end footer -->

  </table>
  <!-- end body -->
</body>
</html>
EMAIL;

					require 'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
					$mail = new PHPMailer();
					$mail->IsSMTP();

					#####################################

					$mail->SMTPAuth   = true;
					$mail->SMTPSecure = "ssl";
					$mail->Host       = "smtp.gmail.com";
					$mail->Port       = 465;
					$mail->Username="test@olineit.com";
					$mail->Password="Tss@bbTS";

					########################################

					$mail->AddAddress('info@olineit.com'); // To
					$mail->SetFrom($email,$name);
					$mail->addCC('ksu@olineit.com','Broad Band Telecom Services Limited (BBTS)'); //CC
					$mail->addBCC('amisalabir@gmail.com','Broad Band Telecom Services Limited (BBTS)'); //BCC
					$mail->AddReplyTo($email,$name); //Reply to
					$mail->Subject = 'New Application Submitted by '.$name;
					$mail->addAttachment("$uploadedFile");
					$message =$emailcontent;
					$mail->MsgHTML($message);

					##################################################################
					// Send email
					//$mail = mail($toEmail, $emailSubject, $message, $headers, $returnpath);
					// If mail sent
					if($mail->Send()){
						$statusMsg = 'Your contact request has been submitted successfully !';
						$msgClass = 'succdiv';
						$postData = '';
						// Delete attachment file from the server
						@unlink($uploadedFile);
						unset($_POST);
						header('Location: index.php?cv=success');
					}else{
						$statusMsg = 'Your application submission failed, please try again.';
					}
				}

			}
		}
	}else{
		$statusMsg = 'Please fill all the fields.';
	}
}
#####################################################

	include("header.php");
?>
<!-- Container (About Section) -->
<div>
	<h1 style="margin-top:90px;">PHPmailer</h1>
	<div id="profile" class="container consame textp">
			<h2 class="text-center" style="color:#D76C42;"></h2><br>
			 <center><div class="devider"></div></center>
			 <br>
			 <!-- ===============Start from here ================ -->
			 <div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div style="border: 1px solid #ddd;padding: 10px;border-radius:6px;" class="careerform">
						<form action="career.php" class="" method="post" enctype="multipart/form-data">
							<input type="hidden" name="submitcv">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="Name">Name</label>
										<input type="text" class="form-control" name="name" placeholder="Enter Name">
									 </div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="Email">Email</label>
										<input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter Email">
									 </div>
								</div>
							</div>
							<div class="form-group">
								<label for="Mobile">Mobile</label>
								<input type="text" class="form-control" name="mobile" placeholder="Enter Mobile Number">
							</div>
							<div class="form-group">
								<label for="Upload">Upload C.V.</label>
								<input type="file" class="form-control fle" name="attachment">
							</div>		  
						  <input type="submit" name="send" value="SEND" class="btn btn-primary">
						</form>
					</div>
				</div>
				<div class="col-md-3"></div>
			 </div>
			<!-- ===============End here ================ -->	
	</div>
</div>
	<?php

	include("footer.php");

	?>