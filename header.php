<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="shortcut icon" href="img/logo/favicon.png">
	
    <title>PHP Mailer</title>

    <!-- Bootstrap core CSS -->
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Bootstrap Dropdown Hover CSS -->
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/bootstrap-dropdownhover.min.css" rel="stylesheet">
	
	<!--<link href="css/slider.css" rel="stylesheet">-->
	<!-- build:css mfb.css -->
    <link href="dist/mfb.css" rel="stylesheet"><!--need one mfb.css-->
    <!-- slick css-->
	<link rel="stylesheet" href="slick/slick.css" type="text/css">
	<link rel="stylesheet" href="slick/slick-theme.css" type="text/css">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="navbar-fixed-top.css" rel="stylesheet">
 	<link rel="shortcut icon" href="img/favicon.png">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	  <script src="vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
	  <!-- Custom -->
	  <link rel="stylesheet" href="vendor/components/jqueryui/themes/base/jquery-ui.css">
	  <script src="vendor/components/jquery/jquery.js"></script>
	  <script src="vendor/components/jqueryui/jquery-ui.js"></script>
  </head>

  <body >
